function calculateTotalSalariesByCountry(data) {
    try {
        let totalSalariesByCountry = {};

        // Iterate through each person in the data array
        for (let i = 0; i < data.length; i++) {
            const person = data[i];
            const country = person.location;

            // Extract the salary string and convert it to a number
            const salary = parseFloat(person.salary.replace('$', ''));

            // If the country already exists in the object, add the salary to its total
            if (totalSalariesByCountry[country]) {
                totalSalariesByCountry[country] += salary;
            } else {
                // If the country doesn't exist, initialize it with the current salary
                totalSalariesByCountry[country] = salary;
            }
        }

        return totalSalariesByCountry;
    } catch (error) {
        console.error('An error occurred:', error);
        return null; // or any other value that indicates error
    }
}


module.exports = calculateTotalSalariesByCountry;

