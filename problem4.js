function calculateTotalSalary(data) {
    try {
        let totalSalary = 0;

        // Iterate through each person in the data array
        for (let i = 0; i < data.length; i++) {
            // Extract the salary string and convert it to a number
            const salary = parseFloat(data[i].salary.replace('$', ''));
            
            // Add the salary to the total
            totalSalary += salary;
        }

        return totalSalary;
    } catch (error) {
        console.error('An error occurred:', error);
        return null; // or any other value that indicates error
    }
}

module.exports = calculateTotalSalary;
