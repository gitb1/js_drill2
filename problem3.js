function convertAndCorrectSalaries(data) {
    try {
        // Iterate through each person in the data array
        for (let i = 0; i < data.length; i++) {
            // Remove the '$' sign and convert the string to a number
            const salary = parseFloat(data[i].salary.replace('$', ''));

            // Correct the salary amount by multiplying it by 10000
            const correctedSalary = salary * 10000;

            // Add the corrected salary as a new key
            data[i].corrected_salary = correctedSalary;
        }
        return data;
    } catch (error) {
        console.error('An error occurred:', error);
        return null; // or any other value that indicates error
    }
}

module.exports = convertAndCorrectSalaries;
