
//Find the sum of all salaries.

const calculateTotalSalary = require('../problem4.js');

// Sample data
const data = require('../js_drill_2.cjs');

// Test the calculateTotalSalary function
const totalSalary = calculateTotalSalary(data);

// Check the total salary
console.log('Total Salary:', totalSalary);
