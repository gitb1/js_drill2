//Convert all the salary values into proper numbers instead of strings.


try {
    const convertSalariesToNumbers = require('../problem2.js');
   
  // Read the contents of the .cjs file
  const data = require('../js_drill_2.cjs');
  
  const convertedData = convertSalariesToNumbers(data);
  console.log(convertedData);
  } catch (error) {
    console.error('Error:', error.message);
  }
  