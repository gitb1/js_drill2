// Find the average salary of based on country. ( Groupd it based on country and then find the average ).

const calculateAverageSalaryByCountry = require('../problem6.js');


const data =require('../js_drill_2.cjs');

// Test the calculateAverageSalaryByCountry function
const averageSalariesByCountry = calculateAverageSalaryByCountry(data);

// Check the average salaries by country
console.log('Average Salaries by Country:', averageSalariesByCountry);