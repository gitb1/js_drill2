
function convertSalariesToNumbers(data) {
 try{

    // Iterate through each person in the data array
    for (let i = 0; i < data.length; i++) {
        // Remove the '$' sign and convert the string to a number
        data[i].salary = parseFloat(data[i].salary.replace('$', ''));
    }
    return data;
  }catch(error){
    console.error('Error:',error.message);
  }
}

module.exports = convertSalariesToNumbers;