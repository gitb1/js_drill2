
function findWebDevelopers(data) {
try{const webDevelopers = [];

// Iterate over each object and check if the first name contains "char"
  for(let i=0;i<data.length;i++){
       
    if (data[i].job && data[i].job.toLowerCase().includes('web developer')) {
        webDevelopers.push(data[i]);
    }
   
   }
  return webDevelopers;

  }catch(error){
       console.error('Error:', error.message);
  }
}
module.exports = findWebDevelopers;