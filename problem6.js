function calculateAverageSalaryByCountry(data) {
    try {
        let totalSalariesByCountry = {};
        let countByCountry = {};

        // Iterate through each person in the data array
        for (let i = 0; i < data.length; i++) {
            const person = data[i];
            const country = person.location;

            // Extract the salary string and convert it to a number
            const salary = parseFloat(person.salary.replace('$', ''));

            // If the country already exists in the object, add the salary and increment the count
            if (totalSalariesByCountry[country]) {
                totalSalariesByCountry[country] += salary;
                countByCountry[country]++;
            } else {
                // If the country doesn't exist, initialize it with the current salary and count
                totalSalariesByCountry[country] = salary;
                countByCountry[country] = 1;
            }
        }

        let averageSalariesByCountry = {};

        // Calculate the average salary for each country
        for (let country in totalSalariesByCountry) {
            averageSalariesByCountry[country] = totalSalariesByCountry[country] / countByCountry[country];
        }

        return averageSalariesByCountry;
    } catch (error) {
        console.error('An error occurred:', error);
        return null; // or any other value that indicates error
    }
}

module.exports = calculateAverageSalaryByCountry;
